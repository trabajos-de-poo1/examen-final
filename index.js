import express from 'express';
const app = express();

class Calculador {
	constructor(a, b, c) {
		this.valor_a = Number(a);
		this.valor_b = Number(b);
		this.valor_c = Number(c);
		this.resultado = undefined;
	};

	obtenerValores() {
		return ({
			valor_a: this.valor_a,
			valor_b: this.valor_b,
			valor_c: this.valor_c
		})
	}

	obtenerValor(valor) {
		return this[valor];
	}
}

class CalculadorBasico extends Calculador {
	constructor(a, b, c) {
		super(a, b, c);
	}

	sumatoria() {
		this.resultado = this.valor_a + this.valor_b + this.valor_c;
		return this.resultado;
	}

	promedio() {
		this.resultado = (this.valor_a + this.valor_b + this.valor_c) / 3;
		return this.resultado;
	}
}

class CalculadorAvanzado extends CalculadorBasico {
	constructor(a, b, c) {
		super(a, b, c);
	}

	menor() {
		return Math.min(this.valor_a, this.valor_b, this.valor_c);
	}

	mayor() {
		return Math.max(this.valor_a, this.valor_b, this.valor_c);
	}
}

// Declarar variable
let calculador;

// Definir valores de la calculadora
app.get('/calculador/valores', (req, res) => {

	const { a, b, c } = req.query;

	calculador = new CalculadorAvanzado(a, b, c);

	res.json({
		ok: true,
		calculador
	})

});

// Sumar los valores
app.get('/calculador/sumatoria', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	const resultado = calculador.sumatoria();

	res.json({
		ok: true,
		resultado
	});

});


// Promedio de los valores
app.get('/calculador/promedio', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	const resultado = calculador.promedio();

	res.json({
		ok: true,
		resultado
	});

});


// Valor mayor
app.get('/calculador/mayor', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	const resultado = calculador.mayor();

	res.json({
		ok: true,
		resultado
	});

});

// Valor menor
app.get('/calculador/menor', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	const resultado = calculador.menor();

	res.json({
		ok: true,
		resultado
	});

});

// Todos los valores
app.get('/calculador/obtener-valores', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	res.json({
		ok: true,
		valores: calculador.obtenerValores()
	});

});

// Todos los valores
app.get('/calculador/valor/:key', (req, res) => {

	if(!calculador) return res.json({ok: false, mensaje: 'No se definieron valores'});

	// Obtenemos valor de la URL
	let key;

	switch(req.params.key) {
		case 'a':
			key = 'valor_a';
			break;
		case 'b':
			key = 'valor_b';
			break;
		case 'c':
			key = 'valor_c';
			break;
		default:
			break;
	}

	if(!key) return res.json({ ok: false, mensaje: 'Elija a, b o c' });

	res.json({
		ok: true,
		[key]: calculador.obtenerValor(key)
	});

});

// Definir puerto
app.listen(3000, () => {

	console.log('Servidor en el puerto ', 3000);

});